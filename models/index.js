const ClientModel = require('./client')
const UserModel = require('./user')

module.exports = {ClientModel, UserModel}