const mongoose = require('mongoose')
const Schema = require('mongoose').Schema

const Client = new Schema({
    userId: {
        type: String,
        unique: true,
        required: true
    },
    clientSecret: {
        type: String,
        required: true
    },
    presentedName: {
        type: String,
        required: true
    },
    redirectUrl: {
        type: String,
        required: true
    }
});

const ClientModel = mongoose.model('Client', Client);

module.exports = ClientModel