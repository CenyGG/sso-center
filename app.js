const config = require('./config').config
const express = require('express')
const routes = require('./routes')
const passport = require('passport')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const mongoose = require("mongoose");
const cookieParser = require('cookie-parser')()

const app = express()
app.set('view engine', 'ejs');

mongoose.connect(config.mongodb.connection, () => {
    console.log("Connected to mongodb")
});

app.use(passport.initialize());
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cookieParser)

app.use('/auth', routes.auth)
app.use('/api', passport.authenticate('jwt', {session: false}), routes.api)

const port = config.port || 3000
app.listen(port, () => {
    console.log(`Listen on port ${port}`)
})