import React, {Component} from 'react';
import logo from '../logo.svg';
import '../css/App.css';
import GeneratedKeys from "./GeneratedKeys";
import {inject, observer} from "mobx-react/index";
import UserCard from "./UserCard";
import {Col, Row} from "antd";

@inject('client')
@inject('user')
@observer
class App extends Component {
    constructor(props) {
        super(props);
        this.checkAuthorized()
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Привeтствуем Вас в центре авторизации.</h1>
                </header>

                {!this.props.user.username ?
                    (<div>
                            <Row type="flex" justify="center" align="middle">
                                <Col span='10'>
                                    <div>
                                        <p className='App-intro2'>  <br />Здесь Вы можете создать свою учетную запись
                                         для нашего совершенного сервиса.
                                         <br /> Вы можете стать нашим пользователем и использовать наше приложение
                                           для аутентификации в других службах.
                                           <br /> Наш сервис позволяет использовать одну учётную запись для <br /> <a href="http://localhost:3002" target="_blank">портала научной конференции</a></p>
                                        <p className='App-intro2'>Good luck and have fun!</p>
                                    </div>
                                </Col>
                            </Row>
                            <Row type="flex" justify="center" align="middle">
                                <Col span='10'>
                                <div >
                                        <div className='App-intro'>Но сначала . . .</div>
                                        <div className='App-intro2'>Вы должны <a href='/login'> login </a> или <a href='/reg'> register </a>
                                        </div>
                                    </div>
                                </Col>
                            </Row>                
                        </div>
                    ) : (
                        <div>
                            <Row type="flex" justify="center" align="middle">
                                <Col span='10'>
                                    <div style={{padding:25}}>
                                        <UserCard/>, how are you doing?
                                    </div>
                                </Col>
                            </Row>
                            <Row type="flex" justify="center" align="middle">
                                <Col span='10'>
                                    <GeneratedKeys />
                                </Col>
                            </Row>
                        </div>
                    )}

            </div>
        );
    }

    checkAuthorized() {
        console.log("checkAuthorized")
        fetch(`/api/user`, {
            method: 'GET',
            credentials: 'include',
        })
            .then((res) => {
                if (res.status !== 200) throw Error("Now authorized")
                return res.json()
            })
            .then((res) => {
                console.log(res)
                if (res.error) return
                this.props.user.username = res.username
                this.props.user.email = res.email
                this.props.user.id = res.id
            }).catch((err) => {
            console.log(err.message)
        })
    }
}

export default App;
