import React from 'react';
import {observer, inject} from "mobx-react";
import {Popover, Button} from 'antd';

@inject('user')
@observer
class UserCard extends React.Component {
    state = {
        visible: false,
    }
    logout = () => {
        this.setState({
            visible: false,
        });
        fetch(`/auth/logout`, {
            method: 'GET',
            'credentials': 'include'
        })
            .then((res) => {
                if (!(res.status === 200)) throw Error("Error while logout")
                this.props.user.id = undefined
                this.props.user.username = undefined
                this.props.user.email = undefined
            })
            .catch((err) => {
                console.log(err.message)
            })
    }
    handleVisibleChange = (visible) => {
        this.setState({visible});
    }

    render() {
        return (
            <Popover
                placement="bottomRight"
                content={<Button type="danger" onClick={this.logout}>Logout</Button>}
                title="Exit?"
                trigger="click"
                visible={this.state.visible}
                onVisibleChange={this.handleVisibleChange}
            >
                <Button type="primary" icon="user" size='large'>{this.props.user.username}</Button>
            </Popover>
        );
    }
}

export default UserCard