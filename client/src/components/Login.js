import {Form, Icon, Input, Button, Checkbox, notification} from 'antd';
import {inject} from 'mobx-react';
import React from 'react';
import {observer} from "mobx-react/index";

const FormItem = Form.Item;

@inject('user')
@observer
class NormalLoginForm extends React.Component {
    render() {
        const user = this.props.user;
        if (user.username) this.props.history.push("/")

        const {getFieldDecorator} = this.props.form;
        return (
            <div className='middle-style'>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <FormItem>
                        {getFieldDecorator('username', {
                            rules: [{required: true, message: 'Please input your username!'}],
                        })(
                            <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                   placeholder="Username"/>
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Please input your Password!'}],
                        })(
                            <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                                   placeholder="Password"/>
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>Remember me</Checkbox>
                        )}
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                        <div className='register'>
                            Or <a href="/reg">register now!</a>
                        </div>

                    </FormItem>
                </Form>
            </div>
        );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);

                fetch(`/auth/login`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    'credentials': 'include',
                    body: JSON.stringify({
                        username: values.username,
                        password: values.password,
                    })
                })
                    .then((res) => {
                        if (res.status !== 200) throw Error('Server error')
                        return res.json()
                    })
                    .then((res) => {
                        if (!res.success) throw Error(res.error)
                        this.props.user.username = res.username
                        this.props.user.email = res.email
                        this.openNotificationWithIcon('success', "Successfully logged in.")
                        setTimeout(() => {
                            this.props.history.push("/")
                        }, 1000)
                    })
                    .catch((err) => {
                        this.openNotificationWithIcon('error', err.message)
                    })

            }
        });
    }

    openNotificationWithIcon(type, descr) {
        notification[type]({
            message: type.toUpperCase(),
            description: descr,
        });
    }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);
export default WrappedNormalLoginForm