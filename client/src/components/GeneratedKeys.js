import {Input, Icon, Button} from 'antd';
import React from 'react';
import {observer, inject} from "mobx-react";
import {observable} from 'mobx';

const InputGroup = Input.Group;

@inject('client')
@observer
class GeneratedKeys extends React.Component {

    constructor(props) {
        super(props)
        fetch('/api/client', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'credentials': 'include'
        })
            .then((res) => {
                if (res.status !== 200) return
                return res.json()
            })
            .then((res) => {
                this.client.id = res._id;
                this.client.clientSecret = res.clientSecret
                this.client.presentedName = res.presentedName
                this.client.redirectUrl = res.redirectUrl
            })
            .catch((err) => {
                console.log(err)
            })
    }

    @observable iconLoading = false;

    enterIconLoading() {
        this.iconLoading = true

        fetch(`/api/client`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'credentials': 'include',
            body: JSON.stringify({
                presentedName: this.client.presentedName,
                redirectUrl: this.client.redirectUrl,
            })
        })
            .then((res) => {
                if (res.status !== 200) throw Error("Error while generating.")
                return res.json()
            })
            .then((res) => {
                this.client.id = res._id;
                this.client.clientSecret = res.clientSecret
                this.iconLoading = false
                console.log(res)
            })
            .catch((err) => {
                this.iconLoading = false
                console.log(err.message)
            })
    }

    handleChange(prop) {
        return (e) => {
            this.client[prop] = e.target.value;
        }
    };

    render() {
        this.client = this.props.client;

        return (
            <div>
                <div className='generate-code-header'> Теперь Вы можете сгенерировать код для Вашего приложения!</div>
                <div className='generate-code-input'>
                    <Input placeholder="Presented client name"
                           value={this.client.presentedName}
                           onChange={this.handleChange('presentedName')}/>
                </div>
                <div className='generate-code-input'>
                    <Input placeholder="Redirect url" addonAfter={<Icon type="setting"/>}
                           value={this.client.redirectUrl}
                           onChange={this.handleChange('redirectUrl')}/>
                </div>
                <div className='generate-code-button'>
                    <Button type="primary"
                            icon="poweroff"
                            loading={this.iconLoading}
                            onClick={this.enterIconLoading.bind(this)}>
                        Generate Key
                    </Button>
                </div>
                <InputGroup style={{marginTop: '2em'}} compact>
                    <Input style={{width: '20%'}} disabled placeholder="Client ID: "/>
                    <Input style={{width: '50%'}} disabled placeholder="Generated client id" value={this.client.id}/>
                </InputGroup>
                <InputGroup style={{marginTop: '1em'}} compact>
                    <Input style={{width: '20%'}} disabled defaultValue="Client key: "/>
                    <Input style={{width: '50%'}} disabled placeholder="Generated client key"
                           value={this.client.clientSecret}/>
                </InputGroup>
            </div>
        )
    };
}

export default GeneratedKeys