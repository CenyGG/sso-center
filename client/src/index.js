import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import App from "./components/App";
import Login from "./components/Login";
import Register from "./components/Register";
import {Provider} from 'mobx-react';
import {observable} from 'mobx';
import './css/index.css';
import 'antd/dist/antd.css';

const stores = {
    @observable
    user: {
        username: null,
        email: null
    },
    @observable
    client: {
        id: null,
        clientSecret: null,
        presentedName: null,
        redirectUrl: null
    }
}

ReactDOM.render(
    <Provider {...stores}>
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={App}/>
                <Route exact path='/login' component={Login}/>
                <Route exact path='/reg' component={Register}/>
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();



