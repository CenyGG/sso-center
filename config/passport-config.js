const passport = require("passport");
const LocalStrategy = require('passport-local').Strategy
const passportJWT = require("passport-jwt")
const JWTStrategy = passportJWT.Strategy
const UserModel = require('../models').UserModel
const ClientModel = require('../models').ClientModel
const config = require('./config.js')


passport.use(new LocalStrategy(
    function (username, password, cb) {
        return UserModel.findOne({username})
            .then(user => {
                console.log(1, user)
                if (!user) {
                    return cb(null, false, {message: 'Incorrect username.'});
                }
                user.verifyPassword(password, (err, res) => {
                    console.log(2, password)
                    if (err) return cb(err)
                    if (res) return cb(null, user, {message: 'Logged In Successfully'})
                    else return cb(null, false, {message: 'Incorrect password.'})
                })
            })
            .catch(cb);
    }
));

const JwtFromCookiesExtractor = function (cookie) {
    return function (req) {
        return req.cookies[cookie]
    }
}

passport.use(new JWTStrategy({
        jwtFromRequest: JwtFromCookiesExtractor('token'),
        secretOrKey: config.jwt.secret
    },
    function (jwtPayload, cb) {
    console.log("jwt", jwtPayload)
        return UserModel.findById(jwtPayload.id)
            .then(user => {
                return cb(null, user);
            })
            .catch(err => {
                return cb(err);
            });
    }
));

const clientStrategy = new LocalStrategy({
        usernameField: 'id',
        passwordField: 'clientSecret'
    },
    function (id, clientSecret, cb) {
        return ClientModel.findById(id)
            .then(client => {
                if (!client) {
                    return cb(null, false, {message: 'Incorrect client.'});
                }
                if (clientSecret === client.clientSecret) return cb(null, client)
                else return cb(null, false, {message: 'Incorrect secret.'})
            })
            .catch(cb);
    }
)
clientStrategy.name = 'client'

passport.use(clientStrategy);