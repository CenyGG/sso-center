require('dotenv').config()

const port = process.env.PORT
const jwt = {
    secret : process.env.JWT_SECRET,
    expiresIn: 10*60
}
const mongodb = {
    connection : process.env.MONGODB_CONNECTION
}

module.exports = {jwt,mongodb,port}