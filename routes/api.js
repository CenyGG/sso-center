const express = require('express');
const router = express.Router();
const ClientModel = require("../models").ClientModel;
const crypto = require('crypto');

router.get('/user', (req, res) => {
    res.json({username: req.user.username, email: req.user.email, id: req.user.id})
})

router.post('/client', (req, response) => {
    let options = {
        userId: req.user.id,
        presentedName: req.body.presentedName,
        redirectUrl: req.body.redirectUrl,
        clientSecret: generateSecret()
    }

    ClientModel.findOne({userId: options.userId})
        .then((res) => {
            if (!res) {

                ClientModel.create(options, (err, client) => {
                    console.log('err', err)
                    console.log('client', client)
                    if (err) return response.status(500).json({error: err.message})
                    response.status(200).json(client)
                })
            } else {

                ClientModel.update({userId: options.userId}, {$set: {...options}}, (err, _res) => {
                    if (err) return response.status(500).json({error: err.message})
                    ClientModel.findOne({userId: options.userId}, (err, client) => {
                        if (err) return response.status(500).json({error: err.message})
                        response.status(200).json(client)
                    })

                })
            }
        })
        .catch((err) => {
            response.status(500).json({error: err.message})
        })
})

router.get('/client', (req, res) => {
    const userId = req.user.id;
    console.log(userId)

    ClientModel.findOne({userId})
        .then((client) => {
            console.log(client)
            if (client) res.json(client)
            else res.status(200).end()
        })
        .catch((err) => {
            console.log(err)
            res.status(500).json({error: err.message})
        })

})

generateSecret = function () {
    console.log('generateSecret')
    return crypto.randomBytes(64).toString('hex')
}

module.exports = router