const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const UserModel = require("../models").UserModel;
const ClientModel = require("../models").ClientModel;
const config = require('../config').config

router.post('/login', function (req, res) {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        console.log(err)
        console.log(user)
        if (err || !user) {
            return res.json({
                success: false,
                user: user
            });
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                res.json({success: false, error: err.message});
            }
            const options = {
                name: user.username,
                id: user.id,
                email: user.email
            }
            const token = jwt.sign(options, config.jwt.secret, {
                expiresIn: config.jwt.expiresIn
            });
            res.cookie("token", token, {expires: new Date(Date.now() + config.jwt.expiresIn * 1000), httpOnly: true})
            res.json({success: true})
        });
    })(req, res);
});

router.post('/reg', function (req, res) {
    if (req.body.password !== req.body.confirm) {
        return res.json({success: false, error: "Wrong confirmation password"})
    }
    UserModel.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }, (err, success) => {
        if (err) return res.json({success: false, error: err.message})
        res.json({success: true})
    })
});

router.get('/logout', function (req, res) {
    res.clearCookie('token')
    res.json({success: true})
});

router.get('/redirect', passport.authenticate('jwt', {failureRedirect: '/login', session: false}), (req, res) => {
    const user = {
        id: req.user.id,
        username: req.user.username,
        email: req.user.email
    }

    console.log('redirect')

    ClientModel.findById(req.query.client_id)
        .then((client) => {
            const sign = jwt.sign(user, client.clientSecret, {expiresIn: config.jwt.expiresIn})
            const render_opts = {
                jwt: sign,
                username: user.username,
                client: client.presentedName,
                client_id: client.id,
                redirect_url: client.redirectUrl,
            }

            res.render('oauth', render_opts)
        })
        .catch((err) => {
            res.end(err.message)
        })
})

module.exports = router
